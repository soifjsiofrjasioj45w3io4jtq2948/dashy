const app = require("express")();
const Token = require("../../model/token");
const cors = require("cors");
const sharp = require("sharp");
const { v4: uuidv4 } = require("uuid");
const Movie = require("../../model/movie");
var AWS = require("aws-sdk");
const bucket = "cdn.streaming-ondemand.xyz";
const path = require("path");
const fs = require("fs");
const { execFile } = require("child_process");

var corsOptions = {
    origin: "https://streaming-ondemand.xyz",
    methods: "POST",
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.post("/upload-movie", cors(corsOptions), function (req, res, next) {
    Token.findById(req.body.token, (err, token) => {
        if (
            err ||
            token.used ||
            token.expires.getTime() <= new Date().getTime() ||
            !req.files ||
            Object.keys(req.files).length !== 2
        ) {
            res.status(403);
            next();
        }

        Token.findByIdAndUpdate(req.body.token, { used: true }, () => {
            res.status(200).json({ status: "valid token!" });
        });

        // The name of the input field (i.e. 'sampleFile') is used to retrieve the uploaded file
        const movie = req.files.movie;
        const thumbnail = req.files.thumbnail;

        // Use the mv() method to place the file somewhere on your server
        const file = req.files.movie.name;
        const filename = uuidv4();
        const input = path.join(__dirname, "..", "convert", "input", file);
        const output = path.join(
            __dirname,
            "..",
            "convert",
            "output",
            "mpd",
            filename
        );

        if (!fs.existsSync(output)) {
            fs.mkdirSync(output);
        }

        console.log("sh ./convert/run.sh " + file + " " + filename);

        sharp(thumbnail.data)
            .resize(291, 163)
            .toFile(
                "convert/output/mpd/" +
                    filename +
                    "/" +
                    filename +
                    "-thumb.jpg",
                function (err) {
                    if (err) console.log(err);
                }
            );

        movie.mv(input, function (err) {
            if (err) {
                fs.unlink(input, (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    }

                    //file removed
                    return res.status(500).send(err);
                });
            } else {
                //file saved
                path.parse(file).name;
                execFile(
                    "sh",
                    ["./convert/run.sh", file, filename],
                    (error, stdout, stderr) => {
                        console.log(stdout);
                        console.log(stderr);
                        if (error !== null) {
                            console.log(`exec error: ${error}`);
                        }

                        uploadFiles = [
                            ".mpd",
                            "-360p.mp4",
                            "-720p.mp4",
                            "-audio.mp4",
                            "-thumb.jpg",
                            "-hq.jpg",
                            "-preview.gif",
                        ];

                        uploadFiles.forEach((variant) => {
                            uploads3(
                                path.join(output, filename + variant),
                                file,
                                filename,
                                req.body.token
                            );
                        });
                    }
                );
            }
        });
    });
});

const s3 = new AWS.S3({ apiVersion: "2006-03-01" });
function uploads3(variant, file, filename, token) {
    var uploadParams = { Bucket: bucket, Key: "", Body: "" };

    var fileStream = fs.createReadStream(variant);
    fileStream.on("error", function (err) {
        console.log("File Error", err);
    });
    uploadParams.Body = fileStream;
    uploadParams.Key = path.basename(variant);

    finished = 0;

    // call S3 to retrieve upload file to specified bucket
    s3.upload(uploadParams, function (err, data) {
        if (err) {
            console.log("Error", err);
        }
        if (data) {
            finished++;

            if (uploadFiles.length === finished) {
                console.log("sh ./convert/cleanup.sh " + file + " " + filename);
                execFile(
                    "sh",
                    ["./convert/cleanup.sh", file, filename],
                    (error, stdout, stderr) => {
                        console.log(stdout);
                        console.log(stderr);
                        if (error !== null) {
                            console.log(`exec error: ${error}`);
                        }
                    }
                );

                Movie.findOneAndUpdate(
                    { uploadToken: token },
                    {
                        visible: true,
                        fileName: filename,
                    },
                    {
                        new: true,
                    }
                ).exec();
            }
        }
    });
}

module.exports = app;
